'use strict';

const cuid = require('cuid');
const Error = require('@scola/error');
const EventHandler = require('@scola/events');

class AbstractConnection extends EventHandler {
  constructor(filters, serial, message) {
    super();

    this.id = cuid();
    this.filters = filters;
    this.messageProvider = message;

    this.serialReceive = serial.get()
      .setProcessor(this.processReceive.bind(this));

    this.serialSend = serial.get()
      .setProcessor(this.processSend.bind(this));

    this.request = null;
    this.response = null;
    this.data = [];
  }

  getId() {
    return this.id;
  }

  getRequest() {
    return this.request;
  }

  getResponse() {
    return this.response;
  }

  open() {
    throw new Error('not_implemented');
  }

  close() {
    throw new Error('not_implemented');
  }

  canSend() {
    throw new Error('not_implemented');
  }

  send(message) {
    this.emit('debug', this, 'send', message);

    return this.serialSend
      .process(this.filters.slice().reverse(), [message])
      .then(this.handleSend.bind(this, message))
      .catch(this.handleSendError.bind(this, message));
  }

  processSend(filter, [message]) {
    this.emit('debug', this, 'processSend', message);
    return filter.get().send(message);
  }

  handleSend() {
    throw new Error('not_implemented');
  }

  handleSendError(message, error) {
    this.emit('error', new Error('transport_message_not_sent', {
      origin: error,
      detail: {
        message
      }
    }));
  }

  handleReceiveData(data) {
    this.emit('debug', this, 'handleReceiveData', data);
    this.data.push(data);
  }

  handleReceiveEnd() {
    this.emit('debug', this, 'handleReceiveEnd', this.data);

    const data = Buffer.concat(this.data);
    const message = this.messageProvider
      .get()
      .setConnection(this)
      .setStatus(this.response.statusCode)
      .setHeaders(this.response.headers)
      .setBody(data);

    this.data = [];

    return this.serialReceive
      .process(this.filters, [message])
      .then(this.handleReceive.bind(this, message))
      .catch(this.handleReceiveError.bind(this, message));
  }

  processReceive(filter, [message]) {
    this.emit('debug', this, 'processReceive', message);
    return filter.get().receive(message);
  }

  handleReceive(message) {
    this.emit('message', message);
  }

  handleReceiveError(message, error) {
    this.emit('error', new Error('transport_message_not_received', {
      origin: error,
      detail: {
        message
      }
    }));
  }

  handleError(error) {
    this.emit('error', new Error('transport_connection_error', {
      origin: error
    }));
  }
}

module.exports = AbstractConnection;
